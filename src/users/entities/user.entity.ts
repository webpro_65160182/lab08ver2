import { Order } from "src/orders/entities/order.entity";
import { Role } from "src/roles/entities/role.entity";
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable, OneToMany } from "typeorm"


@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    gender: string;

    @CreateDateColumn()
    created: Date;

    @UpdateDateColumn()
    updated: Date;
    //หลาย User มีได้หลาย Role
    @ManyToMany(() => Role, (role) => role.users, { cascade: true })
    @JoinTable()
    roles: Role[];

    //1 User มีได้หลาย Order
    @OneToMany(() => Order, (order) => order.user)
    orders: Order[];
}
